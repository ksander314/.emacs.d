(require-package 'color-theme-sanityinc-solarized)
(load-theme 'sanityinc-solarized-dark t)
(provide 'init-theme)
